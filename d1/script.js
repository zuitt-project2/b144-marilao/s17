console.log('Array manipulation')
// Basic Array Structure
// Access Elements in an array - through index

// Two ways to initialize an Array

let array = [1,2,3];
console.log(array);

let arr = new Array(1,2,3);
console.log(arr);
/*
console.log(array[0]);
console.log(array[1]);
console.log(array[2]);
*/
// index = array.length - 1 (formula)

// Array Manipulation

let count = ['one','two','three','four'];
console.log(count.length);
console.log(count[4]); //undefined

// using (=) we can add a new element at the end of an array.
count[4] = "five"
console.log(count);

// 
// count[0] = 'element'
// console.log(count); //it changed the value of first element 0 to element.

// Push method array.push()
// console.log(count.push('element'))
	// add element at the end of an array 
count.push('element')
console.log(count)

function pushMethod(element){
	return count.push(element)
}

pushMethod('six')
pushMethod('seven')
pushMethod('eight')

console.log(count);

// Pop method array.pop()
	// removes the last element of an array
	// we cannot use pop method to remove element no matter what
count.pop();

console.log(count)

function popMethod(){
	count.pop()
}
popMethod()

console.log(count)

// How to add element at the beginning of an array? 
	// Unshift method array.unshift()
		// adds element at the beginning of an array
	count.unshift('hugot')
	console.log(count)

	function unshiftmethod(name){
		return count.unshift(name)
	}

	unshiftmethod('zero')
	console.log(count)

	// Can we remove element at the beginning of an array?
		// Shift method array.shift()
		count.shift()
		console.log(count)

		function shiftmethod(){
			return count.shift()
		}
		shiftmethod()

		console.log(count)

		// Sort method array.sort()
		let nums = [15,32,61,130,230,13,34];
		nums.sort();
		console.log(nums)

		// sort nums in ascending order
		nums.sort(
			function(a, b){
				return a - b
			}	
		)
		console.log(nums)

		// sort nums in descending order
		nums.sort(
			function(a, b){
				return b - a
			}	
		)
		console.log(nums)

		// Reverse method array.reverse()
		nums.reverse()

		console.log(nums)

		/*Splice and Slice*/
		// Splice method  array.splice()
			// returns array of omitted elements
			// first parameter - index where to start omitting element
			// second parameter - #of elements to be omitted starting from first parameter
			// Third parameter - elements to be added in place of the omitted elements
		// console.log(count)

		/*let newSplice = count.splice(1);
		console.log(newSplice)
		console.log(count)*/

		/*let newSplice = count.splice(1, 2);
		console.log(newSplice);
		console.log(count)*/
/*
		let newSplice = count.splice(1, 2, 'a1', 'b2', 'c3')
		console.log(newSplice)
		console.log(count)
*/

	// Slice method aray.slice(start, end)
		
		// First parameter - index where to begin omitting elements
		// Second parameter - # of elements to be omitted (index - 1)

// let newSlice = count.slice(1);
// console.log(newSlice);
// console.log(count)

let newSlice = count.slice(3,5);
console.log(newSlice);
console.log(count)

// concat method  array.concat()
	// use to merge two or more arrays
console.log(count)
console.log(nums)
let animals = ['bird','cat','dog','fish'];

let newConcat = count.concat(nums, animals)
console.log(newConcat);

// Join method array.join() // "", " ", "-"
let meal = ['rice','steak','juice'];

let newJoin = meal.join()
console.log(newJoin)

newJoin = meal.join("")
console.log(newJoin)

newJoin = meal.join(" ")
console.log(newJoin)

newJoin = meal.join("-")
console.log(newJoin)


//toString method 
console.log(nums)
	console.log(typeof nums[3])

	let newString = nums.toString()
	console.log(typeof newString)

/*Accessors*/
let countries = ['US','PH','CAN','PH','SG','HK','PH','NZ'];
	// indexOf() array.indexOf()
		// finds the index of a given element where it is 'first' found.
	let index = countries.indexOf('PH')
	console.log(index);

	// if element is non existing, return is -1
	let example = countries.indexOf('AU')
	console.log(example);

// lastIndexOf()
let lastIndex = countries.lastIndexOf('PH');
console.log(lastIndex);

/*Iterators*/

	// .forEach()	array.forEach()
	// map()		array.map()

let days = ['mon','tue','wed','thu','fri','sat','sun'];

	days.forEach(
		function(element){
			console.log(element)
		}
	)

	// map
		// return a copy of an array from the original array which can be manipulated
	let mapDays = days.map(function(day){
		return `${day} is the day of the week`
	})
	console.log(mapDays);
	console.log(days)

	// Mini activity

	/*let days2 = days.map(function(day){
		return day;
	})
	console.log(days2);
	console.log(days)
	
	days2.forEach(
		function(day){
			console.log(day)
		}

	)*/
	let days2 = [];
	console.log(days2)

	days.forEach(function(day){
		days2.push(day)
	})
	console.log(days2)

	// filter	array.filter(cb())
	console.log(nums)

	let newFilter = nums.filter(function(num){
		return num < 50
	})
	console.log(newFilter)

	// includes array.includes()
		// returns boolean value if a word is exisiting in an array
	console.log(animals)

	let newIncludes = animals.includes('dog')
	console.log(newIncludes)

	// Mini activity

	let fruits = ['apple','orange','banana','melon'];
	console.log(fruits)

	function activity(name){

		if(fruits.includes(name) == true){
			return `${name} is found`
		}else {
			return `${name} is not found`
		}
	}
	console.log(activity('apple'))
	console.log(activity('tamur'))
	console.log(activity('khalid'))


	// every(cb())
		// returns boolean value like AND logic
		// returns true only if "all elements" passed the given condition
	let newEvery = nums.every(function(num){
		return ( num > 50)
	})
	console.log(newEvery);


	// some(cb())
		// boolean like OR logic
	let newSome = nums.some(function(num){
		return ( num > 50)
	})
	console.log(newSome);

	// let newSome2 = nums.some(num => num > 50)
	// console.log(newSome2); // can be written like this as well

	// reduce(cb(<previous>, <current>))
	let newReduce = nums.reduce(function(a, b){
		return a + b
		// return b - a
	})
	console.log(newReduce)

	// to get the average of nums array
		// total all the elements
		// divide it by total # of elements
		// console.log(newReduce/ nums.length)

		let average = newReduce/ nums.length
// ______________________________________________
		// toFixed(# of decimals) - returns string

		console.log(average.toFixed(2))

		// parseInt() or parseFloat()

		console.log(parseInt(average.toFixed(2)))
		console.log(parseFloat(average.toFixed(2)))
		// parseInt() or parseFloat

